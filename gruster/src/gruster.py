import datetime
import os
import re
from collections import defaultdict

import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.widgets import CheckButtons
from src.utils.colors import Colors, ConditionalColors
from src.utils.markers import Markers

plt.style.use('seaborn-darkgrid')


class Data(object):
    def __init__(self, data):
        self.graph_type = None
        self.xy_points = None
        self.no_of_clusters = None
        self.cluster_assignments = None
        self.is_correctly_classified = None
        self.adjacency_matrix = None

        if "graph_type" in data:
            self.graph_type = data["graph_type"]
        if "xy_points" in data:
            self.xy_points = data["xy_points"]
        if "no_of_clusters" in data:
            self.no_of_clusters = data["no_of_clusters"]
        if "cluster_assignments" in data:
            self.cluster_assignments = data["cluster_assignments"]
        if "adjacency_matrix" in data:
            self.adjacency_matrix = data["adjacency_matrix"]
        if "is_correctly_classified" in data:
            self.is_correctly_classified = data["is_correctly_classified"]


class GrusterPlotter(object):
    def __init__(self, data):

        self._grid_specification = gridspec.GridSpec(1, 2, width_ratios=[5, 1])
        self._figure = plt.figure()
        self._axes = self._figure.add_subplot(self._grid_specification[0, 0])
        self._check_box_axes = self._figure.add_subplot(self._grid_specification[0, 1])
        self._check_box_axes.set_aspect("equal")

        self._figure.set_size_inches(14, 10)
        self._axes.set_aspect('equal')

        self._figure.canvas.mpl_connect("pick_event", lambda event: self.on_pick(event))
        self._check_button = []
        self._subplot_instances = defaultdict()
        self.plotting_functions = defaultdict()
        self.plotting_functions["_plot_scatter_points"] = self._plot_scatter_points
        self.plotting_functions["_plot_arrows"] = self._plot_arrows
        self.plotting_functions["_plot_cluster_assignments"] = self._plot_cluster_assignments

        self._data = Data(data)

    def plot(self):
        if self._data.xy_points is not None:
            self._subplot_instances["layer_scatter_points"] = self._plot_scatter_points()
        if self._data.adjacency_matrix is not None:
            self._subplot_instances["layer_arrows"] = self._plot_arrows()
            self._subplot_instances["layer_arrow_labels"] = self._plot_arrow_labels()
        if self._data.cluster_assignments is not None:
            self._subplot_instances["layer_cluster_assignments"] = self._plot_cluster_assignments()

        self._color_scatter_points()

        check_box_labels = self._subplot_instances.keys()
        self._check_button = CheckButton(self._check_box_axes, check_box_labels,
                                         [self._get_subplot_visibility(l) for l in check_box_labels],
                                         self._switch_subplot_visibility)

    def save_figure(self, root_path, figure_name):
        filename = type(self).__name__ + str(datetime.datetime.now())
        filename = re.sub('[^A-Za-z0-9]+', '_', filename)
        path_to_file = os.path.join(root_path, figure_name + filename + '.png')
        self._figure.savefig(path_to_file, dpi=300)

    def _plot_scatter_points(self):
        custom_color = Colors()
        custom_marker = Markers()
        list_of_patch_collection_obj = []
        index = 0
        for x, y in zip(self._data.xy_points[:, 0], self._data.xy_points[:, 1]):
            list_of_patch_collection_obj.append(self._axes.scatter(x, y,
                                                                   marker=custom_marker[
                                                                       self._data.cluster_assignments[index]],
                                                                   color=custom_color[
                                                                       self._data.cluster_assignments[index]],
                                                                   s=500,
                                                                   lw=0,
                                                                   picker=True, label=index,
                                                                   visible=True))
            index += 1
        return list_of_patch_collection_obj

    def _calculate_arrow_head_dimensions(self):
        ylim = self._axes.get_ylim()
        arrow_head_width = (ylim[1] - ylim[0]) / 100
        arrow_head_length = arrow_head_width * 2
        return arrow_head_width, arrow_head_length

    def _plot_arrows(self):
        x = self._data.xy_points[:, 0]
        y = self._data.xy_points[:, 1]
        arrow_head_width, arrow_head_length = self._calculate_arrow_head_dimensions()
        list_of_arrow_obj = []

        for (i, j), weight in np.ndenumerate(self._data.adjacency_matrix):
            if i is j:
                continue
            # arrow_color = arrow_colors[i]
            arrow_color = 'b'
            arrow_obj = self._axes.arrow(
                x[i], y[i],
                dx=x[j] - x[i],
                dy=y[j] - y[i],
                head_width=arrow_head_width,
                head_length=arrow_head_length,
                fc=arrow_color, ec=arrow_color,
                head_starts_at_zero=False, length_includes_head=True,
                alpha=0.5
            )
            list_of_arrow_obj.append(arrow_obj)

        return list_of_arrow_obj

    def _color_scatter_points(self):
        cluster_color_getter = ConditionalColors(
            [plt.get_cmap("Greens")(v) for v in np.arange(0.5, 1, 0.1)] +
            [plt.get_cmap("Blues")(v) for v in np.arange(0.5, 1, 0.1)],
            [plt.get_cmap("autumn")(v) for v in np.arange(0, 1, 0.1)]
        )

        for i, is_correctly_classified in enumerate(self._data.is_correctly_classified):
            cluster_color = cluster_color_getter.get_color_with_condition(
                self._data.cluster_assignments[i], is_correctly_classified
            )
            self._subplot_instances["layer_scatter_points"][i].set_color(cluster_color)

    def _plot_arrow_labels(self):
        x = self._data.xy_points[:, 0]
        y = self._data.xy_points[:, 1]
        annotation_obj = []
        for (i, j), weight in np.ndenumerate(self._data.adjacency_matrix):
            if self._data.adjacency_matrix[i, j] == 0.0:
                continue
            else:
                annotation_obj.append(self._axes.annotate(
                    str(self._data.adjacency_matrix[i, j]), ((x[j] + x[i]) / 2.0, (y[j] + y[i]) / 2.0)))
        return annotation_obj

    def on_pick(self, event):
        index = int(event.artist.get_label())
        print(self._data.xy_points[index, :])

    def _plot_cluster_assignments(self):
        annotation_obj = []
        for txt, xy in zip(self._data.cluster_assignments, self._data.xy_points):
            annotation_obj.append(self._axes.annotate(txt, (xy[0], xy[1])))
        return annotation_obj

    def _display_clustering_information(self):
        txt = 'No. of clusters: ' + str(self._data.no_of_clusters)
        self._figure.text(0, 0, txt, fontdict=None)

    def _get_subplot_visibility(self, subplot_name):
        subplot_instance = self._subplot_instances[subplot_name]
        if type(subplot_instance) is list:
            subplot_is_visible = any(map(lambda x: x.get_visible(), subplot_instance))
        else:
            subplot_is_visible = subplot_instance.get_visible()
        return subplot_is_visible

    def _switch_subplot_visibility(self, subplot_name):
        subplot_instance = self._subplot_instances[subplot_name]
        if type(subplot_instance) is list:
            for item in subplot_instance:
                item.set_visible(not item.get_visible())
        else:
            subplot_instance.set_visible(not self._subplot_instances[subplot_name].get_visible())


class CheckButton(object):
    def __init__(self, axis, subplot_names, subplot_is_visible, switch_subplot_visibility):
        self._rax = axis
        self._switch_subplot_visibility = switch_subplot_visibility

        self._check = CheckButtons(self._rax, subplot_names, subplot_is_visible)
        self._check.on_clicked(self.on_click)

    def on_click(self, subplot_name):
        self._switch_subplot_visibility(subplot_name)
        self._rax.figure.canvas.draw_idle()


def _main():
    data = {}
    data["xy_points"] = np.random.normal(1, 2, [5, 2])
    data["cluster_assignments"] = [1, 1, 1, 2, 2]
    data["is_correctly_classified"] = [True, False, True, True, True]
    data["no_of_clusters"] = len(np.unique(data["cluster_assignments"]))
    data["adjacency_matrix"] = np.random.normal(0, 1, [5, 5])

    p = GrusterPlotter(data)
    p.plot()
    p.save_figure('visualizations', 'test_figure')
    plt.show()


if __name__ == '__main__':
    _main()
