from collections import defaultdict
import numpy as np
import seaborn as sns
sns.reset_orig()


class Markers(object):

    def __init__(self, markers=None):
        if markers is None:
            self._markers = 'o', 'v', '^', '<', '>', 's', 'p', '*', 'h', 'H', 'D', 'd', 'P', 'X'
        else:
            self._markers = markers

        self._current_index = 0
        self._number_of_markers = len(self._markers)
        self._marker_map = defaultdict(self._next_marker)

    def _next_marker(self):
        marker = self._markers[self._current_index]
        self._current_index = (self._current_index + 1) % self._number_of_markers
        return marker

    def set_value_to_marker(self, value, marker):
        self._marker_map[value] = marker

    def set_bad_marker(self, marker):
        self._marker_map[np.nan] = marker

    def __getitem__(self, item):
        if np.isnan(item):
            return self._marker_map[np.nan]
        return self._marker_map[int(item)]
