from collections import defaultdict
import numpy as np
import seaborn as sns

sns.reset_orig()


class Colors(object):
    def __init__(self, colors=None):
        if colors is None:
            self._colors = sns.color_palette("Set2", 10)
        else:
            self._colors = colors

        self._current_index = 0
        self._number_of_colors = len(self._colors)
        self._color_map = defaultdict(self._next_color)

    def _next_color(self):
        color = self._colors[self._current_index]
        self._current_index = (self._current_index + 1) % self._number_of_colors
        return color

    def set_value_to_color(self, value, color):
        self._color_map[value] = color

    def set_bad_color(self, color):
        self._color_map[np.nan] = color

    def __getitem__(self, item):
        if isinstance(item, tuple):
            item = tuple([np.nan if np.isnan(i) else i for i in item])
        elif np.isnan(item):
            return self._color_map[np.nan]

        return self._color_map[item]


class ConditionalColors(object):
    def __init__(self, colors_a, colors_b):
        self._colors_list = [Colors(colors_a), Colors(colors_b)]

    def get_color_with_condition(self, item, condition):
        return self._colors_list[int(condition)][item]
